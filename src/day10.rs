#[derive(Debug, Clone)]
enum Pipe {
    Vertical,
    Horizontal,
    NE,
    NW,
    SW,
    SE,
    Ground,
    Start,
}
impl From<char> for Pipe {
    fn from(character: char) -> Self {
        match character {
            '|' => Pipe::Vertical,
            '-' => Pipe::Horizontal,
            'L' => Pipe::NE,
            'J' => Pipe::NW,
            '7' => Pipe::SW,
            'F' => Pipe::SE,
            '.' => Pipe::Ground,
            'S' => Pipe::Start,
            _ => {
                panic!("{} is not a valid character", character);
            }
        }
    }
}
#[derive(Debug)]
struct Details {
    map: Vec<Vec<Pipe>>,
    start: (usize, usize),
}

#[derive(Debug)]
pub struct Processed {
    part1: Details,
}

pub fn generator_1(input: &str) -> Processed {
    let mut processed = Processed { part1: Details { map: vec![], start: (0, 0) } };

    for (i, line) in input.split('\n').enumerate() {
        let mut row = vec![];
        for (j, character) in line.chars().enumerate() {
            if character == 'S' {
                processed.part1.start = (i, j);
            }
            row.push(Pipe::from(character));
        }
        processed.part1.map.push(row);
    }

    processed
}

pub fn part1_1(input: &Processed) -> i64 {
    for start in look_around(&input.part1.start, &input.part1.map) {
        //println!("Start: {:?} {:?}", start, &input.part1.map[start.0][start.1]);

        let mut current = input.part1.start;
        let mut next_position = vec![start];

        let mut steps = 0;

        loop {
            if next_position.is_empty() {
                break;
            }

            let test_position = next_position.remove(0);
            //println!("Start: {:?} {:?}", test_position, &input.part1.map[test_position.0][test_position.1]);

            match &input.part1.map[test_position.0][test_position.1] {
                Pipe::Ground => {
                    continue;
                }
                Pipe::Start => {
                    steps += 1;

                    steps /= 2;
                    return steps;
                }
                _ => {}
            }

            //println!("here1");

            let mut tmp = vec![];
            for next_routes in look_around(&test_position, &input.part1.map) {
                if next_routes.0 == current.0 && next_routes.1 == current.1 {
                    continue;
                }
                //println!("next: {:?} {:?}", next_routes, &input.part1.map[next_routes.0][next_routes.1]);
                tmp.push(next_routes);
            }

            if tmp.len() != 1 {
                // not vald route
                continue;
            }

            //println!("here3");

            current = test_position;

            for position in tmp {
                next_position.push(position);
            }

            steps += 1;
        }
    }

    0
}

fn look_around(start: &(usize, usize), map: &Vec<Vec<Pipe>>) -> Vec<(usize, usize)> {
    let mut valid_paths = vec![];

    let (mut up, mut down, mut left, mut right) = (false, false, false, false);
    match map[start.0][start.1] {
        Pipe::Vertical => {
            up = true;
            down = true;
        }
        Pipe::Horizontal => {
            left = true;
            right = true;
        }
        Pipe::NE => {
            up = true;
            right = true;
        }
        Pipe::NW => {
            up = true;
            left = true;
        }
        Pipe::SW => {
            down = true;
            left = true;
        }
        Pipe::SE => {
            down = true;
            right = true;
        }
        Pipe::Ground => {}
        Pipe::Start => {
            up = true;
            down = true;
            left = true;
            right = true;
        }
    }

    // up
    if up && start.0 > 0 {
        match map[start.0 - 1][start.1] {
            Pipe::Vertical | Pipe::SW | Pipe::SE | Pipe::Start => {
                valid_paths.push((start.0 - 1, start.1));
            }
            _ => {}
        }
    }

    // down
    if down && start.0 < (map.len() - 1) {
        match map[start.0 + 1][start.1] {
            Pipe::Vertical | Pipe::NW | Pipe::NE | Pipe::Start => {
                // println!("here!");
                valid_paths.push((start.0 + 1, start.1));
            }
            _ => {}
        }
    }

    // left
    if left && start.1 > 0 {
        match map[start.0][start.1 - 1] {
            Pipe::Horizontal | Pipe::NE | Pipe::SE | Pipe::Start => {
                valid_paths.push((start.0, start.1 - 1));
            }
            _ => {}
        }
    }

    // right
    if right && start.1 < (map[start.0].len() - 1) {
        match map[start.0][start.1 + 1] {
            Pipe::Horizontal | Pipe::NW | Pipe::SW | Pipe::Start => {
                valid_paths.push((start.0, start.1 + 1));
            }
            _ => {}
        }
    }

    valid_paths
}

pub fn part2_1(input: &Processed) -> i64 {
    for start in look_around(&input.part1.start, &input.part1.map) {
        //println!("Start: {:?} {:?}", start, &input.part1.map[start.0][start.1]);

        let mut flooded = vec![vec![Pipe::Ground; input.part1.map[0].len()]; input.part1.map.len()];

        let mut current = input.part1.start;
        let mut next_position = vec![start];

        let mut complete = false;
        loop {
            if next_position.is_empty() {
                break;
            }

            let test_position = next_position.remove(0);
            //println!("Start: {:?} {:?}", test_position, &input.part1.map[test_position.0][test_position.1]);

            match &input.part1.map[test_position.0][test_position.1] {
                Pipe::Ground => {
                    continue;
                }
                Pipe::Start => {
                    complete = true;
                    break;
                }
                _ => {}
            }

            //println!("here1");

            let mut tmp = vec![];
            for next_routes in look_around(&test_position, &input.part1.map) {
                if next_routes.0 == current.0 && next_routes.1 == current.1 {
                    continue;
                }
                //println!("next: {:?} {:?}", next_routes, &input.part1.map[next_routes.0][next_routes.1]);
                tmp.push(next_routes);
            }

            if tmp.len() != 1 {
                // not vald route
                continue;
            }

            current = test_position;

            flooded[current.0][current.1] = input.part1.map[current.0][current.1].clone();
            //flooded[current.0][current.1] = Pipe::Vertical;
            for position in tmp {
                flooded[position.0][position.1] = input.part1.map[position.0][position.1].clone();
                //flooded[position.0][position.1] = Pipe::Vertical;
                next_position.push(position);
            }
        }

        if complete {
            let mut count = 0;
            // process filled map now
            for line in flooded {
                let mut count_line = 0;
                let mut verticals = 0;
                for character in &line {
                    //print!("{:?}", character);

                    match character {
                        Pipe::Ground => {
                            if verticals > 0 && verticals % 2 != 0 {
                                count_line += 1;
                            }
                        }
                        Pipe::Vertical | Pipe::NE | Pipe::NW => {
                            verticals += 1;
                        }
                        _ => {}
                    }
                }
                //println!("{} {:?}", count_line, &line);
                count += count_line;
            }
            return count;
        }
    }

    0
}

#[cfg(test)]
mod tests {
    use super::*;

    const EXAMPLE: &str = ".....
.S-7.
.|.|.
.L-J.
.....";

    const EXAMPLE2: &str = "..F7.
.FJ|.
SJ.L7
|F--J
LJ...";

    const EXAMPLE3: &str = "...........
.S-------7.
.|F-----7|.
.||.....||.
.||.....||.
.|L-7.F-J|.
.|..|.|..|.
.L--J.L--J.
...........";

    const EXAMPLE4: &str = "..........
.S------7.
.|F----7|.
.||....||.
.||....||.
.|L-7F-J|.
.|..||..|.
.L--JL--J.
..........";

    const EXAMPLE5: &str = ".F----7F7F7F7F-7....
.|F--7||||||||FJ....
.||.FJ||||||||L7....
FJL7L7LJLJ||LJ.L-7..
L--J.L7...LJS7F-7L7.
....F-J..F7FJ|L7L7L7
....L7.F7||L7|.L7L7|
.....|FJLJ|FJ|F7|.LJ
....FJL-7.||.||||...
....L---J.LJ.LJLJ...";

    const EXAMPLE6: &str = "FF7FSF7F7F7F7F7F---7
L|LJ||||||||||||F--J
FL-7LJLJ||||||LJL-77
F--JF--7||LJLJ7F7FJ-
L---JF-JLJ.||-FJLJJ7
|F|F-JF---7F7-L7L|7|
|FFJF7L7F-JF7|JL---7
7-L-JL7||F7|L7F-7F7|
L.L7LFJ|||||FJL7||LJ
L7JLJL-JLJLJL--JLJ.L";

    #[test]
    fn part1_test() {
        assert_eq!(part1_1(&generator_1(EXAMPLE)), 4);
    }

    #[test]
    fn part1_test2() {
        assert_eq!(part1_1(&generator_1(EXAMPLE2)), 8);
    }

    #[test]
    fn part2_test() {
        assert_eq!(part2_1(&generator_1(EXAMPLE3)), 4);
    }

    #[test]
    fn part2_test2() {
        assert_eq!(part2_1(&generator_1(EXAMPLE4)), 4);
    }

    #[test]
    fn part2_test3() {
        assert_eq!(part2_1(&generator_1(EXAMPLE5)), 8);
    }

    #[test]
    fn part2_test4() {
        assert_eq!(part2_1(&generator_1(EXAMPLE6)), 10);
    }
}
