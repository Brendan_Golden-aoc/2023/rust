#[derive(Debug)]
struct Card {
    id: usize,
    have: Vec<u8>,
    winning: Vec<u8>,
}

pub struct Processed {
    part1: Vec<Card>,
}

pub fn generator_1(input: &str) -> Processed {
    let mut processed = Processed { part1: vec![] };

    for line in input.split('\n') {
        let mut card = Card { id: 0, have: vec![], winning: vec![] };
        let tmp = line.replace("Card ", "");
        let split = tmp.split(':').collect::<Vec<&str>>();
        card.id = split[0].trim().parse::<usize>().unwrap();

        let mumbers = split[1].trim().split('|').collect::<Vec<&str>>();
        card.have = str_to_vec::<u8>(mumbers[0]);
        card.winning = str_to_vec::<u8>(mumbers[1]);
        processed.part1.push(card);
    }

    processed
}

fn str_to_vec<T: std::str::FromStr>(numbers: &str) -> Vec<T> {
    let mut result = vec![];

    for number in numbers.trim().split(' ').collect::<Vec<&str>>() {
        let trimmed = number.trim();

        if let Ok(x) = trimmed.parse::<T>() {
            result.push(x);
        }
    }

    result
}

pub fn part1_1(input: &Processed) -> i64 {
    let mut count = 0;

    for card in &input.part1 {
        let mut matches = 0;

        for number in &card.have {
            if card.winning.contains(number) {
                matches += 1;
            }
        }
        if matches > 0 {
            //println!("{} {} {:?}",matches, 2_i64.pow(matches), &card.have);

            count += 2_i64.pow(matches - 1);
        }
    }

    count
}

pub fn part2_1(input: &Processed) -> i64 {
    let mut card_counts = vec![1; input.part1.len()];
    for card in &input.part1 {
        let mut matches = 0;

        for number in &card.have {
            if card.winning.contains(number) {
                matches += 1;
            }
        }

        if matches > 0 {
            let copies = card_counts[card.id - 1];
            for count in card_counts.iter_mut().skip(card.id).take(matches) {
                *count += copies;
            }
        }
    }

    card_counts.iter().sum()
}

#[cfg(test)]
mod tests {
    use super::*;

    const EXAMPLE: &str = "Card 1: 41 48 83 86 17 | 83 86  6 31 17  9 48 53
Card 2: 13 32 20 16 61 | 61 30 68 82 17 32 24 19
Card 3:  1 21 53 59 44 | 69 82 63 72 16 21 14  1
Card 4: 41 92 73 84 69 | 59 84 76 51 58  5 54 83
Card 5: 87 83 26 28 32 | 88 30 70 12 93 22 82 36
Card 6: 31 18 13 56 72 | 74 77 10 23 35 67 36 11";

    #[test]
    fn part1_test() {
        assert_eq!(part1_1(&generator_1(EXAMPLE)), 13);
    }

    #[test]
    fn part2_test() {
        assert_eq!(part2_1(&generator_1(EXAMPLE)), 30);
    }
}
