pub struct Processed {
    part1: Vec<Vec<char>>,
    part2: Vec<(Vec<char>, Vec<char>)>,
}

pub fn generator_1(input: &str) -> Processed {
    let mut processed = Processed { part1: vec![], part2: vec![] };

    let conversion = [("one", '1'), ("two", '2'), ("three", '3'), ("four", '4'), ("five", '5'), ("six", '6'), ("seven", '7'), ("eight", '8'), ("nine", '9')];

    for line in input.split('\n') {
        let tmp = line.chars().collect::<Vec<char>>();

        // forwards
        let mut tmp2 = vec![];
        let mut i = 0;
        'outer: while i < tmp.len() {
            //println!("{} {}",i,  tmp[i]);
            for (from, to) in &conversion {
                if i + from.len() > line.len() {
                    continue;
                }
                let tmp = &tmp[i..(i + from.len())];
                if tmp == from.chars().collect::<Vec<char>>() {
                    tmp2.push(*to);
                    i += from.len();
                    continue 'outer;
                }
            }

            tmp2.push(tmp[i]);
            i += 1;
        }

        let mut line_rev = tmp.clone();
        line_rev.reverse();

        let mut tmp3 = vec![];

        let mut j = tmp.len() - 1;
        'outer: while (j as isize) >= 0 {
            //println!("{} {}",i,  tmp[i]);
            for (from, to) in &conversion {
                if (j as isize - from.len() as isize) < 0 {
                    continue;
                }
                let tmp = &tmp[(j - from.len() + 1)..=j];
                //println!("{} {:?} {:?}", tmp == from.chars().collect::<Vec<char>>(), tmp , from.chars().collect::<Vec<char>>());
                if tmp == from.chars().collect::<Vec<char>>() {
                    tmp3.push(*to);
                    j -= from.len();
                    continue 'outer;
                }
            }

            tmp3.push(tmp[j]);
            if j == 0 {
                break;
            }
            j -= 1;
        }

        processed.part1.push(tmp);
        processed.part2.push((tmp2, tmp3))
    }

    processed
}

pub fn part1_1(input: &Processed) -> i64 {
    let mut sum = 0;
    for line in &input.part1 {
        let mut first = '0';
        let mut second = '0';
        for char in line {
            if char.is_numeric() {
                first = *char;
                break;
            }
        }
        for char in line.iter().rev() {
            if char.is_numeric() {
                second = *char;
                break;
            }
        }

        match format!("{}{}", first, second).parse::<i64>() {
            Ok(x) => sum += x,
            Err(err) => {
                println!("{}", err)
            }
        }
    }

    sum
}

pub fn part2_1(input: &Processed) -> i64 {
    let mut sum = 0;
    for line in &input.part2 {
        let mut first = '0';
        let mut second = '0';
        for char in &line.0 {
            if char.is_numeric() {
                first = *char;
                break;
            }
        }
        for char in &line.1 {
            if char.is_numeric() {
                second = *char;
                break;
            }
        }

        // println!("{} {} {:?}", first, second, line);
        match format!("{}{}", first, second).parse::<i64>() {
            Ok(x) => sum += x,
            Err(err) => {
                println!("{}", err)
            }
        }
    }

    sum
}

pub struct Processed2 {
    part1: Vec<Vec<char>>,
    part2: Vec<Vec<char>>,
}

pub fn generator_2(input: &str) -> Processed2 {
    let mut processed = Processed2 { part1: vec![], part2: vec![] };

    let conversion = [("one", '1'), ("two", '2'), ("three", '3'), ("four", '4'), ("five", '5'), ("six", '6'), ("seven", '7'), ("eight", '8'), ("nine", '9')];

    // I essensially discard teh original input and create an array of arrays of chars that I know are numerals.
    for line in input.split('\n') {
        let mut part1 = vec![];
        let mut part2 = vec![];

        // loop through each character of each line.
        for (i, character) in line.chars().enumerate() {
            if character.is_numeric() {
                // part 1 is only concerned with the numebers, so create a new "line" of just the numbers.
                part1.push(character);

                // we care about the numbers in part 2 as well
                part2.push(character);
            } else {
                // loop through the conversionj list above.
                // if the next few characters match any of the strings add the number to the new array
                for (from, to) in conversion {
                    if i + from.len() > line.len() {
                        continue;
                    }

                    if &line[i..(i + from.len())] == from {
                        part2.push(to);
                    }
                }
            }
        }

        // add each row to the processed output
        processed.part1.push(part1);
        processed.part2.push(part2);
    }

    processed
}

pub fn part1_2(input: &Processed2) -> i64 {
    sub_2(&input.part1)
}

pub fn part2_2(input: &Processed2) -> i64 {
    sub_2(&input.part2)
}

fn sub_2(input: &Vec<Vec<char>>) -> i64 {
    let mut sum = 0;
    for line in input {
        let first = line[0];
        let second = line[line.len() - 1];

        // nieve way of converting two number characters into a number, first converting to a string then parsing that string.
        match format!("{}{}", first, second).parse::<i64>() {
            Ok(x) => sum += x,
            Err(err) => {
                println!("{}", err)
            }
        }
    }

    sum
}

pub fn part1_3(input: &Processed2) -> i64 {
    sub_3(&input.part1)
}

pub fn part2_3(input: &Processed2) -> i64 {
    sub_3(&input.part2)
}

fn sub_3(input: &Vec<Vec<char>>) -> i64 {
    let mut sum = 0;
    for line in input {
        // in part 1 and 2 all we care about is the first and teh last number, so pull them out of the array.
        let first = line[0].to_digit(10).unwrap_or_default() as i64;
        let second = line[line.len() - 1].to_digit(10).unwrap_or_default() as i64;

        // since I know they are numbers all I have to do is to use the proper places.
        sum += (first * 10) + second;
    }

    sum
}

#[cfg(test)]
mod tests {
    use super::*;

    const EXAMPLE: &str = "1abc2
pqr3stu8vwx
a1b2c3d4e5f
treb7uchet";

    const EXAMPLE2: &str = "two1nine
eightwothree
abcone2threexyz
xtwone3four
4nineeightseven2
zoneight234
7pqrstsixteen";

    #[test]
    fn part1_test() {
        assert_eq!(part1_1(&generator_1(EXAMPLE)), 142);
    }

    #[test]
    fn part2_test() {
        assert_eq!(part2_1(&generator_1(EXAMPLE2)), 281);
    }

    #[test]
    fn part1_2_test() {
        assert_eq!(part1_2(&generator_2(EXAMPLE)), 142);
    }

    #[test]
    fn part2_2_test() {
        assert_eq!(part2_2(&generator_2(EXAMPLE2)), 281);
    }
}
