use std::collections::HashMap;

#[derive(Debug)]
struct Details {
    instructions: Vec<char>,
    nodes: HashMap<String, (String, String)>,
}

#[derive(Debug)]
pub struct Processed {
    part1: Details,
}

pub fn generator_1(input: &str) -> Processed {
    let mut processed = Processed { part1: Details { instructions: vec![], nodes: Default::default() } };

    let mut lines = input.split('\n');
    if let Some(line) = lines.next() {
        processed.part1.instructions = line.chars().collect();
    }

    // skip the blank line
    lines.next();

    for line in lines {
        let name = line[0..3].to_owned();
        let left = line[7..10].to_owned();
        let right = line[12..15].to_owned();
        processed.part1.nodes.insert(name, (left, right));
    }

    processed
}

pub fn part1_1(input: &Processed) -> usize {
    let mut steps = 0;

    let mut last = "AAA";

    loop {
        if last == "ZZZ" {
            break;
        }

        let instruction = input.part1.instructions[steps % input.part1.instructions.len()];
        if let Some(nodes) = input.part1.nodes.get(last) {
            match instruction {
                'L' => last = &nodes.0,
                'R' => last = &nodes.1,
                _ => {}
            }
        }

        steps += 1;
    }

    steps
}

pub fn part2_1(input: &Processed) -> usize {
    let mut steps = vec![];
    for node in input.part1.nodes.keys() {
        //println!("{}", node);
        if node.ends_with('A') {
            steps.push(part2_sub(&input.part1, node));
        }
    }

    let mut accumulator = 0;

    for step in steps {
        if accumulator == 0 {
            accumulator = step;
        } else {
            accumulator = lcm(accumulator, step);
        }
    }

    accumulator
}

fn lcm(existing: usize, next: usize) -> usize {
    existing * next / gcd(existing, next)
}

fn gcd(existing: usize, next: usize) -> usize {
    let mut max = existing;
    let mut min = next;
    if min > max {
        std::mem::swap(&mut max, &mut min);
    }

    loop {
        let res = max % min;
        if res == 0 {
            return min;
        }

        max = min;
        min = res;
    }
}

fn part2_sub(details: &Details, first: &str) -> usize {
    let mut step = 0;

    let mut last = first;

    loop {
        if last.ends_with('Z') {
            break;
        }

        let instruction = details.instructions[step % details.instructions.len()];
        if let Some(nodes) = details.nodes.get(last) {
            match instruction {
                'L' => last = &nodes.0,
                'R' => last = &nodes.1,
                _ => {}
            }
        }

        step += 1;
    }

    step
}

#[cfg(test)]
mod tests {
    use super::*;

    const EXAMPLE: &str = "RL

AAA = (BBB, CCC)
BBB = (DDD, EEE)
CCC = (ZZZ, GGG)
DDD = (DDD, DDD)
EEE = (EEE, EEE)
GGG = (GGG, GGG)
ZZZ = (ZZZ, ZZZ)";

    const EXAMPLE2: &str = "LLR

AAA = (BBB, BBB)
BBB = (AAA, ZZZ)
ZZZ = (ZZZ, ZZZ)";

    const EXAMPLE3: &str = "LR

11A = (11B, XXX)
11B = (XXX, 11Z)
11Z = (11B, XXX)
22A = (22B, XXX)
22B = (22C, 22C)
22C = (22Z, 22Z)
22Z = (22B, 22B)
XXX = (XXX, XXX)";

    #[test]
    fn part1_test() {
        assert_eq!(part1_1(&generator_1(EXAMPLE)), 2);
    }

    #[test]
    fn part1_test2() {
        assert_eq!(part1_1(&generator_1(EXAMPLE2)), 6);
    }

    #[test]
    fn part2_test() {
        assert_eq!(part2_1(&generator_1(EXAMPLE3)), 6);
    }
}
