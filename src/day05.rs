#[derive(Debug)]
struct Range {
    dest: isize,
    src: isize,
    range: isize,
}

#[derive(Debug)]
struct Details {
    seeds: Vec<isize>,
    seed_soil: Vec<Range>,
    soil_fertilizer: Vec<Range>,
    fertilizer_water: Vec<Range>,
    water_light: Vec<Range>,
    light_temperature: Vec<Range>,
    temperature_humidity: Vec<Range>,
    humidity_location: Vec<Range>,
}

pub struct Processed {
    part1: Details,
}

pub fn generator_1(input: &str) -> Processed {
    let mut processed = Processed {
        part1: Details {
            seeds: vec![],
            seed_soil: vec![],
            soil_fertilizer: vec![],
            fertilizer_water: vec![],
            water_light: vec![],
            light_temperature: vec![],
            temperature_humidity: vec![],
            humidity_location: vec![],
        },
    };

    let mut segments = input.split("\n\n");

    if let Some(seeds) = segments.next() {
        let line = seeds.replace("seeds: ", "");
        processed.part1.seeds = str_to_vec(&line);
    }

    if let Some(map) = segments.next() {
        let mut map_split = map.split('\n');
        // skip header
        map_split.next();

        for line in map_split {
            processed.part1.seed_soil.push(str_to_range(line));
        }
    }

    if let Some(map) = segments.next() {
        let mut map_split = map.split('\n');
        // skip header
        map_split.next();

        for line in map_split {
            processed.part1.soil_fertilizer.push(str_to_range(line));
        }
    }

    if let Some(map) = segments.next() {
        let mut map_split = map.split('\n');
        // skip header
        map_split.next();

        for line in map_split {
            processed.part1.fertilizer_water.push(str_to_range(line));
        }
    }

    if let Some(map) = segments.next() {
        let mut map_split = map.split('\n');
        // skip header
        map_split.next();

        for line in map_split {
            processed.part1.water_light.push(str_to_range(line));
        }
    }

    if let Some(map) = segments.next() {
        let mut map_split = map.split('\n');
        // skip header
        map_split.next();

        for line in map_split {
            processed.part1.light_temperature.push(str_to_range(line));
        }
    }

    if let Some(map) = segments.next() {
        let mut map_split = map.split('\n');
        // skip header
        map_split.next();

        for line in map_split {
            processed.part1.temperature_humidity.push(str_to_range(line));
        }
    }

    if let Some(map) = segments.next() {
        let mut map_split = map.split('\n');
        // skip header
        map_split.next();

        for line in map_split {
            processed.part1.humidity_location.push(str_to_range(line));
        }
    }

    processed
}

fn str_to_vec<T: std::str::FromStr>(numbers: &str) -> Vec<T> {
    let mut result = vec![];

    for number in numbers.trim().split(' ').collect::<Vec<&str>>() {
        let trimmed = number.trim();

        if let Ok(x) = trimmed.parse::<T>() {
            result.push(x);
        }
    }

    result
}

fn str_to_range(numbers: &str) -> Range {
    let mut result = vec![];

    for number in numbers.trim().split(' ').collect::<Vec<&str>>() {
        let trimmed = number.trim();

        if let Ok(x) = trimmed.parse() {
            result.push(x);
        }
    }

    Range { dest: result[0], src: result[1], range: result[2] }
}

pub fn part1_1(input: &Processed) -> isize {
    let mut count = isize::MAX;

    //println!("{:?}",  &input.part1.seed_soil);
    for seed_tmp in &input.part1.seeds {
        let seed = *seed_tmp;
        let soil = part_1_get_next(seed, &input.part1.seed_soil);
        let fertilizer = part_1_get_next(soil, &input.part1.soil_fertilizer);
        let water = part_1_get_next(fertilizer, &input.part1.fertilizer_water);
        let light = part_1_get_next(water, &input.part1.water_light);
        let temperature = part_1_get_next(light, &input.part1.light_temperature);
        let humidity = part_1_get_next(temperature, &input.part1.temperature_humidity);
        let location = part_1_get_next(humidity, &input.part1.humidity_location);

        //println!("Seed {seed}, soil {soil}, fertilizer {fertilizer}, water {water}, light {light}, temperature {temperature}, humidity {humidity}, location {location}");

        if location < count {
            count = location;
        }
    }

    count
}

fn part_1_get_next(last: isize, range: &Vec<Range>) -> isize {
    let mut next = last;
    for mapping in range {
        if mapping.src > last {
            continue;
        }

        if (mapping.src + mapping.range) >= last {
            next += mapping.dest - mapping.src;
            break;
        }
    }
    next
}

pub fn part2_1(input: &Processed) -> isize {
    let mut location = 0;

    let mut seed_range = vec![];

    for (index, range) in input.part1.seeds.iter().enumerate() {
        if index % 2 == 0 {
            continue;
        }
        let start = input.part1.seeds[index - 1];
        seed_range.push((start, start + range));
    }

    'outer: loop {
        let humidity = part_2_get_last(location, &input.part1.humidity_location);
        let temperature = part_2_get_last(humidity, &input.part1.temperature_humidity);
        let light = part_2_get_last(temperature, &input.part1.light_temperature);
        let water = part_2_get_last(light, &input.part1.water_light);
        let fertilizer = part_2_get_last(water, &input.part1.fertilizer_water);
        let soil = part_2_get_last(fertilizer, &input.part1.soil_fertilizer);
        let seed = part_2_get_last(soil, &input.part1.seed_soil);
        //println!("Seed {seed}, soil {soil}, fertilizer {fertilizer}, water {water}, light {light}, temperature {temperature}, humidity {humidity}, location {location}");
        for range in &seed_range {
            if seed >= range.0 && seed < range.1 {
                break 'outer;
            }
        }
        location += 1;
    }

    location
}

fn part_2_get_last(next: isize, range: &Vec<Range>) -> isize {
    let mut mapped = next;

    //println!("{}  {:?}",next,  range);

    for mapping in range {
        if next >= (mapping.dest) && next < (mapping.dest + mapping.range) {
            mapped -= mapping.dest - mapping.src;
            break;
        }
    }
    mapped
}

#[cfg(test)]
mod tests {
    use super::*;

    const EXAMPLE: &str = "seeds: 79 14 55 13

seed-to-soil map:
50 98 2
52 50 48

soil-to-fertilizer map:
0 15 37
37 52 2
39 0 15

fertilizer-to-water map:
49 53 8
0 11 42
42 0 7
57 7 4

water-to-light map:
88 18 7
18 25 70

light-to-temperature map:
45 77 23
81 45 19
68 64 13

temperature-to-humidity map:
0 69 1
1 0 69

humidity-to-location map:
60 56 37
56 93 4";

    #[test]
    fn part1_test() {
        assert_eq!(part1_1(&generator_1(EXAMPLE)), 35);
    }

    #[test]
    fn part2_test() {
        assert_eq!(part2_1(&generator_1(EXAMPLE)), 46);
    }
}
