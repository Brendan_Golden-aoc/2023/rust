#[derive(Debug)]
struct Details {
    map: Vec<Vec<char>>,
}

#[derive(Debug)]
pub struct Processed {
    part1: Details,
}

pub fn generator_1(input: &str) -> Processed {
    let mut processed = Processed { part1: Details { map: vec![] } };

    for line in input.split('\n') {
        processed.part1.map.push(line.chars().collect());
    }

    processed
}

fn expander(map: &Vec<Vec<char>>, expansion: usize) -> Vec<(usize, usize)> {
    let mut galaxies = get_galaxies(map);

    for width in (0..map[0].len()).rev() {
        let mut col_empty = true;
        for row in map {
            if row[width] == '#' {
                col_empty = false;
                break;
            }
        }
        if col_empty {
            for galaxy in galaxies.iter_mut() {
                if galaxy.1 >= width {
                    galaxy.1 += expansion - 1;
                }
            }
        }
    }

    for (i, line) in map.iter().enumerate().rev() {
        let mut row_empty = true;
        for character in line {
            if character == &'#' {
                row_empty = false;
                break;
            }
        }

        if row_empty {
            for galaxy in galaxies.iter_mut() {
                if galaxy.0 >= i {
                    galaxy.0 += expansion - 1;
                }
            }
        }
    }
    galaxies
}

pub fn part1_1(input: &Processed) -> usize {
    // doubles in size, 1 -> 2
    sub(input, 2)
}

fn sub(input: &Processed, expansion: usize) -> usize {
    let galaxies = expander(&input.part1.map, expansion);

    let mut counter = 0;
    for from in 0..galaxies.len() {
        for to in (from + 1)..galaxies.len() {
            counter += distance(&galaxies[from], &galaxies[to]);
        }
    }

    counter
}

fn get_galaxies(expanded: &[Vec<char>]) -> Vec<(usize, usize)> {
    let mut result = vec![];
    for (i, row) in expanded.iter().enumerate() {
        for (j, space) in row.iter().enumerate() {
            if space == &'#' {
                result.push((i, j));
            }
        }
    }
    result
}

fn distance(from: &(usize, usize), to: &(usize, usize)) -> usize {
    let vert = if from.0 > to.0 { from.0 - to.0 } else { to.0 - from.0 };

    let hor = if from.1 > to.1 { from.1 - to.1 } else { to.1 - from.1 };

    vert + hor
}

pub fn part2_1(input: &Processed) -> usize {
    sub(input, 1000000)
}

#[cfg(test)]
mod tests {
    use super::*;

    const EXAMPLE: &str = "...#......
.......#..
#.........
..........
......#...
.#........
.........#
..........
.......#..
#...#.....";

    #[test]
    fn part1_test() {
        assert_eq!(part1_1(&generator_1(EXAMPLE)), 374);
    }

    #[test]
    fn part2_test1() {
        assert_eq!(sub(&generator_1(EXAMPLE), 10), 1030);
    }

    #[test]
    fn part2_test2() {
        assert_eq!(sub(&generator_1(EXAMPLE), 100), 8410);
    }
}
