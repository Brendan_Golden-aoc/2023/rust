use std::collections::HashMap;
#[derive(Debug)]
enum Command {
    Dash,
    Equals(usize),
}

#[derive(Debug)]
struct Step<'a> {
    label: &'a str,
    command: Command,
}

#[derive(Debug)]
pub struct Processed<'a> {
    part1: Vec<&'a str>,
    part2: Vec<Step<'a>>,
}

pub fn generator_1(input: &str) -> Processed {
    let mut processed = Processed { part1: vec![], part2: vec![] };

    for line in input.split(',') {
        processed.part1.push(line);

        let mut part2 = line.split(['-', '=']);
        let mut step = Step { label: "", command: Command::Dash };

        if let Some(label) = part2.next() {
            step.label = label;
        }

        if let Some(x) = part2.next() {
            if let Ok(number) = x.parse() {
                step.command = Command::Equals(number);
            }
        }

        processed.part2.push(step);
    }

    processed
}

pub fn part1_1(input: &Processed) -> usize {
    let mut sum = 0;

    for item in &input.part1 {
        sum += hasher(item);
    }

    sum
}

fn hasher(input: &str) -> usize {
    let mut current = 0;

    for character in input.chars() {
        current += (character as u8) as usize;
        current *= 17;
        current %= 256;
    }

    current
}

struct Lens<'a> {
    label: &'a str,
    power: usize,
}

pub fn part2_1(input: &Processed) -> usize {
    let mut boxes: HashMap<usize, Vec<Lens>> = HashMap::new();

    // set up teh boxes
    for i in 0..256 {
        boxes.insert(i, vec![]);
    }

    for step in &input.part2 {
        let box_number = hasher(step.label);
        match step.command {
            Command::Dash => {
                if let Some(x) = boxes.get_mut(&box_number) {
                    x.retain(|lens| lens.label != step.label);
                }
            }
            Command::Equals(power) => {
                if let Some(x) = boxes.get_mut(&box_number) {
                    let mut modified = false;
                    for lens in &mut *x {
                        if lens.label == step.label {
                            modified = true;
                            lens.power = power;
                            break;
                        }
                    }
                    if !modified {
                        x.push(Lens { label: step.label, power })
                    }
                }
            }
        }
    }

    let mut sum = 0;

    for i in 0..256 {
        if let Some(box_details) = boxes.get(&i) {
            for (position, lens) in box_details.iter().enumerate() {
                sum += (i + 1) * (position + 1) * lens.power;
            }
        }
    }

    sum
}

#[cfg(test)]
mod tests {
    use super::*;

    const EXAMPLE: &str = "HASH";

    const EXAMPLE2: &str = "rn=1,cm-,qp=3,cm=2,qp-,pc=4,ot=9,ab=5,pc-,pc=6,ot=7";

    #[test]
    fn hasher_test() {
        assert_eq!(hasher(EXAMPLE), 52);
    }

    #[test]
    fn part1_test() {
        assert_eq!(part1_1(&generator_1(EXAMPLE2)), 1320);
    }

    #[test]
    fn part2_test() {
        assert_eq!(part2_1(&generator_1(EXAMPLE2)), 145);
    }
}
