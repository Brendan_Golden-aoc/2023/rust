use std::collections::HashMap;

pub struct Processed {
    part1: Vec<Vec<char>>,
}

pub fn generator_1(input: &str) -> Processed {
    let mut processed = Processed { part1: vec![] };

    for line in input.split('\n') {
        processed.part1.push(line.chars().collect())
    }
    processed
}

pub fn part1_1(input: &Processed) -> i64 {
    let mut count = 0;

    for (i, line) in input.part1.iter().enumerate() {
        let mut part = false;
        let mut number = vec![];

        for (j, character) in line.iter().enumerate() {
            if character.is_numeric() {
                number.push(character);
                let (symbols, next) = check_around(&input.part1, i, j);
                if !symbols.is_empty() && !part {
                    part = true;
                }

                //println!("{:?} {:?} {}", &number, symbols, next);
                if !next {
                    if part {
                        let tmp = num_array_to_num(number);
                        //println!("{}", tmp);
                        count += tmp;
                    }

                    number = vec![];
                    part = false;
                }
            }
        }
    }

    count
}

struct Around {
    character: char,
    position: Position,
}

fn check_around(input: &Vec<Vec<char>>, i: usize, j: usize) -> (Vec<Around>, bool) {
    let next_numeric = if j < input[i].len() - 1 { input[i][j + 1].is_numeric() } else { false };

    let mut symbols = vec![];
    for x in -1_isize..=1 {
        let peek_i = i as isize + x;
        if peek_i < 0 {
            continue;
        }
        if peek_i >= input.len() as isize {
            continue;
        }

        let peek_i_u = peek_i as usize;

        for y in -1_isize..=1 {
            let peek_j = j as isize + y;

            // if x == y {
            //     continue;
            // }
            if peek_j < 0 {
                continue;
            }
            if peek_j >= input[peek_i_u].len() as isize {
                continue;
            }

            let peek_j_u = peek_j as usize;

            let cell = input[peek_i_u][peek_j_u];
            //println!("{} {} {} {}  {}",i, j, x, y, cell);
            if !cell.is_numeric() && cell != '.' {
                symbols.push(Around { character: cell, position: Position { x: peek_i_u, y: peek_j_u } })
            }
        }
    }

    (symbols, next_numeric)
}

#[derive(Clone)]
struct Position {
    x: usize,
    y: usize,
}

impl PartialEq for Position {
    fn eq(&self, other: &Self) -> bool {
        self.x == other.x && self.y == other.y
    }
}

pub fn part2_1(input: &Processed) -> i64 {
    let mut count = 0;

    let mut gears: HashMap<String, Vec<i64>> = HashMap::new();
    for (i, line) in input.part1.iter().enumerate() {
        let mut part = false;
        let mut number = vec![];

        let mut gears_tmp = vec![];
        for (j, character) in line.iter().enumerate() {
            if character.is_numeric() {
                number.push(character);
                let (symbols, next) = check_around(&input.part1, i, j);

                for character in &symbols {
                    if character.character == '*' {
                        gears_tmp.push(character.position.clone())
                    }
                }

                if !symbols.is_empty() && !part {
                    part = true;
                }

                if !next {
                    if part {
                        let part_number = num_array_to_num(number);

                        gears_tmp.sort_by_key(|d| d.x);
                        gears_tmp.sort_by_key(|d| d.y);
                        gears_tmp.dedup();

                        for gear in gears_tmp {
                            if let Some(x) = gears.get_mut(&format!("{}_{}", gear.x, gear.y)) {
                                x.push(part_number);
                            } else {
                                gears.insert(format!("{}_{}", gear.x, gear.y), vec![part_number]);
                            }
                        }
                    }

                    number = vec![];
                    part = false;
                    gears_tmp = vec![];
                }
            }
        }
    }

    for values in gears.values() {
        if values.len() != 2 {
            continue;
        }
        let mut ratio = 1;
        for value in values {
            ratio *= *value;
        }
        count += ratio;
    }

    count
}

fn num_array_to_num(number: Vec<&char>) -> i64 {
    let mut part_number = 0;

    for (index, num) in number.iter().enumerate() {
        part_number += num.to_digit(10).unwrap_or_default() as i64 * (10_i64.pow((number.len() - 1 - index) as u32));
    }
    part_number
}

#[cfg(test)]
mod tests {
    use super::*;

    const EXAMPLE: &str = "467..114..
...*......
..35..633.
......#...
617*......
.....+.58.
..592.....
......755.
...$.*....
.664.598..";

    // from https://www.reddit.com/r/adventofcode/comments/189q9wv/2023_day_3_another_sample_grid_to_use/

    const EXAMPLE2: &str = "12.......*..
+.........34
.......-12..
..78........
..*....60...
78.........9
.5.....23..$
8...90*12...
............
2.2......12.
.*.........*
1.1..503+.56";

    #[test]
    fn part1_test() {
        assert_eq!(part1_1(&generator_1(EXAMPLE)), 4361);
    }

    #[test]
    fn part2_test() {
        assert_eq!(part2_1(&generator_1(EXAMPLE)), 467835);
    }

    #[test]
    fn part1_test_2() {
        assert_eq!(part1_1(&generator_1(EXAMPLE2)), 925);
    }

    #[test]
    fn part2_test_2() {
        assert_eq!(part2_1(&generator_1(EXAMPLE2)), 6756);
    }
}
