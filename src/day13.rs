#[derive(Debug)]
struct Mirror {
    mirror: Vec<Vec<usize>>,
}

#[derive(Debug)]
pub struct Processed {
    part1: Vec<Mirror>,
}

pub fn generator_1(input: &str) -> Processed {
    let mut processed = Processed { part1: vec![] };

    for mirror in input.split("\n\n") {
        //println!("{}", mirror);
        let mut details = Mirror { mirror: vec![] };
        for line in mirror.lines() {
            let mut tmp = vec![];

            for character in line.chars() {
                match character {
                    '#' => tmp.push(1),
                    _ => tmp.push(0),
                }
            }

            details.mirror.push(tmp);
        }
        processed.part1.push(details);
    }

    processed
}

pub fn part1_1(input: &Processed) -> usize {
    let mut counter = 0;
    for mirror in &input.part1 {
        let (vertical, horizontal) = part1_sub(mirror, true);

        //println!("{:?} {:?}", vertical, horizontal);
        if let Some(v) = vertical {
            counter += v;
        }

        if let Some(h) = horizontal {
            counter += h * 100;
        }
    }
    counter
}

fn part1_sub(mirror: &Mirror, part1: bool) -> (Option<usize>, Option<usize>) {
    let horizontal = part1_sub_sub(&mirror.mirror, part1);
    let transpose = transpose(mirror.mirror.clone());
    let vertical = part1_sub_sub(&transpose, part1);
    // let vertical = None;
    (vertical, horizontal)
}

// from https://stackoverflow.com/a/74793053
fn transpose<T>(original: Vec<Vec<T>>) -> Vec<Vec<T>> {
    assert!(!original.is_empty());
    let mut transposed = (0..original[0].len()).map(|_| vec![]).collect::<Vec<_>>();

    for original_row in original {
        for (item, transposed_row) in original_row.into_iter().zip(&mut transposed) {
            transposed_row.push(item);
        }
    }

    transposed
}

fn part1_sub_sub(mirror: &Vec<Vec<usize>>, part1: bool) -> Option<usize> {
    for col in 1..=mirror.len() {
        let bottom = mirror.len() - col;
        let range = if col < bottom { col } else { bottom };

        if range < 1 {
            continue;
        }
        let mut mirrored = true;
        let mut rock = false;
        let mut off_by_one = false;
        println!("{} {}", col, bottom);
        'outer: for item in 0..=range {
            println!("{} {} {} {}", col, range, col - item, col + item - 1);

            let row1 = mirror[col - item].clone();
            let row2 = mirror[col + item - 1].clone();

            println!("{:?}", row1);
            println!("{:?}", row2);
            if part1 {
                for i in 0..row1.len() {
                    if row1[i] != row2[i] {
                        mirrored = false;
                        break 'outer;
                    }
                    if row1[i] == 1 {
                        rock = true;
                    }
                }
            } else {
                let sum1 = row1.iter().sum::<usize>() as isize;
                let sum2 = row1.iter().sum::<usize>() as isize;

                if sum1 > 0 {
                    rock = true;
                }
                if sum2 > 0 {
                    rock = true;
                }

                if sum1 != sum2 && (sum1 - sum2).abs() == 1 {
                    if !off_by_one {
                        off_by_one = true;
                    } else {
                        mirrored = false;
                        break 'outer;
                    }
                }
            }
        }

        if mirrored && rock {
            // println!("{col}");

            return Some(col);
        }
    }

    None
}

pub fn part2_1(input: &Processed) -> usize {
    let mut counter = 0;
    for mirror in &input.part1 {
        let (vertical, horizontal) = part1_sub(mirror, false);

        println!("{:?} {:?}", vertical, horizontal);
        if let Some(v) = vertical {
            counter += v;
        }

        if let Some(h) = horizontal {
            counter += h * 100;
        }
    }
    counter
}

#[cfg(test)]
mod tests {
    use super::*;

    const EXAMPLE: &str = "#.##..##.
..#.##.#.
##......#
##......#
..#.##.#.
..##..##.
#.#.##.#.

#...##..#
#....#..#
..##..###
#####.##.
#####.##.
..##..###
#....#..#";

    const EXAMPLE3: &str = "#.##..
.#....";

    const EXAMPLE4: &str = "..
..
..";

    const EXAMPLE5: &str = "#..#
.##.
.##.
#..#";

    #[test]
    fn part1_test() {
        assert_eq!(part1_1(&generator_1(EXAMPLE)), 405);
    }

    #[test]
    fn part1_test3() {
        assert_eq!(part1_1(&generator_1(EXAMPLE3)), 0);
    }

    #[test]
    fn part1_test4() {
        assert_eq!(part1_1(&generator_1(EXAMPLE4)), 0);
    }

    #[test]
    fn part1_test5() {
        assert_eq!(part1_1(&generator_1(EXAMPLE5)), 202);
    }

    const EXAMPLE6: &str = "##.####
######.
######.
######.
######.
..##...
#.##.##
#....#.
#....#.";

    #[test]
    fn part1_test6() {
        assert_eq!(part1_1(&generator_1(EXAMPLE6)), 800);
    }
    #[test]
    fn part2_test() {
        assert_eq!(part2_1(&generator_1(EXAMPLE)), 400);
    }
}
