mod day01;
mod day02;
mod day03;
mod day04;
mod day05;
mod day06;
mod day07;
mod day08;
mod day09;
mod day10;
mod day11;
mod day12;
mod day13;
mod day14;
mod day15;

aoc_main::main! {
    year 2023;
    day01 : generator_1 => part1_1, part2_1;
    day01 : generator_2 => part1_2, part2_2;
    day01 : generator_2 => part1_3, part2_3;
    day02 : generator_1 => part1_1, part2_1;
    day03 : generator_1 => part1_1, part2_1;
    day04 : generator_1 => part1_1, part2_1;
    day05 : generator_1 => part1_1, part2_1;
    day06 : generator_1 => part1_1, part1_2, part2_1,  part2_2;
    day07 : generator_1 => part1_1, part2_1;
    day08 : generator_1 => part1_1, part2_1;
    day09 : generator_1 => part1_1, part1_2, part1_3, part2_1,  part2_2,  part2_3;
    day10 : generator_1 => part1_1, part2_1;
    day11 : generator_1 => part1_1, part2_1;
    day12 : generator_1 => part1_1, part2_1;
    day13 : generator_1 => part1_1, part2_1;
    day14 : generator_1 => part1_1, part2_1;
    day15 : generator_1 => part1_1, part2_1;
}
