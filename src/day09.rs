#[derive(Debug)]
pub struct Processed {
    part1: Vec<Vec<i64>>,
}

pub fn generator_1(input: &str) -> Processed {
    let mut processed = Processed { part1: vec![] };

    for line in input.split('\n') {
        let mut line_new = vec![];
        for number in line.split(' ') {
            if let Ok(x) = number.parse() {
                line_new.push(x);
            }
        }
        processed.part1.push(line_new);
    }

    processed
}

pub fn part1_1(input: &Processed) -> i64 {
    let mut sum = 0;
    for reading in &input.part1 {
        sum += sub_general(reading).0;
    }

    sum
}

pub fn part2_1(input: &Processed) -> i64 {
    let mut sum = 0;
    for reading in &input.part1 {
        sum += sub_general(reading).1;
    }
    sum
}

fn sub_general(prev: &Vec<i64>) -> (i64, i64) {
    let mut current = vec![];
    let mut zeros = true;
    for (i, number) in prev.iter().enumerate() {
        if i == 0 {
            continue;
        }
        let next = *number - prev[i - 1];
        if next != 0 {
            zeros = false;
        }
        current.push(next);
    }

    if zeros {
        (prev[prev.len() - 1], prev[0])
    } else {
        (sub_general(&current).0 + prev[prev.len() - 1], prev[0] - sub_general(&current).1)
    }
}

pub fn part1_2(input: &Processed) -> i64 {
    let mut sum = 0;
    for reading in &input.part1 {
        sum += sub_general2(reading).1;
    }

    sum
}

pub fn part2_2(input: &Processed) -> i64 {
    let mut sum = 0;
    for reading in &input.part1 {
        sum += sub_general2(reading).0;
    }
    sum
}

fn sub_general2(initial: &[i64]) -> (i64, i64) {
    let mut current = initial.to_owned();
    let mut prev = vec![];

    loop {
        let mut zeros = true;

        // keep tack of the start and end
        prev.push((current[0], current[current.len() - 1]));

        let mut next = vec![];
        for (i, number) in current.iter().enumerate() {
            if i == 0 {
                continue;
            }
            let next_number = *number - current[i - 1];
            if next_number != 0 {
                zeros = false;
            }
            next.push(next_number);
        }

        // shift over to this
        current = next;

        if zeros {
            break;
        }
    }

    let mut result = (0, 0);
    prev.reverse();

    for item in prev {
        result.0 = item.0 - result.0;
        result.1 += item.1;
    }

    result
}

pub fn part1_3(input: &Processed) -> i64 {
    let mut sum = 0;
    for reading in &input.part1 {
        sum += sub_general3(reading).1;
    }

    sum
}

pub fn part2_3(input: &Processed) -> i64 {
    let mut sum = 0;
    for reading in &input.part1 {
        sum += sub_general3(reading).0;
    }
    sum
}

fn sub_general3(initial: &[i64]) -> (i64, i64) {
    let mut length = initial.len();
    let mut current = initial.to_owned();
    let mut result = (0, 0);
    let mut even = true;

    loop {
        let mut zeros = true;

        result.0 += if even { current[0] } else { -current[0] };
        result.1 += current[length - 1];
        even = !even;

        for i in 1..length {
            let next_number = current[i] - current[i - 1];
            if next_number != 0 {
                zeros = false;
            }
            current[i - 1] = next_number;
        }

        // reduce length for next one
        length -= 1;

        if zeros {
            break;
        }
    }

    result
}

#[cfg(test)]
mod tests {
    use super::*;

    const EXAMPLE: &str = "0 3 6 9 12 15
1 3 6 10 15 21
10 13 16 21 30 45";

    #[test]
    fn part1_test() {
        assert_eq!(part1_1(&generator_1(EXAMPLE)), 114);
    }

    #[test]
    fn part2_test() {
        assert_eq!(part2_1(&generator_1(EXAMPLE)), 2);
    }

    #[test]
    fn part1_test2() {
        assert_eq!(part1_2(&generator_1(EXAMPLE)), 114);
    }

    #[test]
    fn part2_test2() {
        assert_eq!(part2_2(&generator_1(EXAMPLE)), 2);
    }

    #[test]
    fn part1_test3() {
        assert_eq!(part1_3(&generator_1(EXAMPLE)), 114);
    }

    #[test]
    fn part2_test3() {
        assert_eq!(part2_3(&generator_1(EXAMPLE)), 2);
    }
}
