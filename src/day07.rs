use std::cmp::Ordering;
use std::slice::Iter;

#[derive(Debug, PartialEq, Eq, Clone, Ord, PartialOrd, Copy)]
enum Cards {
    A,
    K,
    Q,
    J,
    T,
    _9,
    _8,
    _7,
    _6,
    _5,
    _4,
    _3,
    _2,
}
impl From<char> for Cards {
    fn from(card: char) -> Self {
        match card {
            'A' => Cards::A,
            'K' => Cards::K,
            'Q' => Cards::Q,
            'J' => Cards::J,
            'T' => Cards::T,
            '9' => Cards::_9,
            '8' => Cards::_8,
            '7' => Cards::_7,
            '6' => Cards::_6,
            '5' => Cards::_5,
            '4' => Cards::_4,
            '3' => Cards::_3,
            '2' => Cards::_2,
            _ => Cards::_2,
        }
    }
}

#[derive(Debug, PartialEq, Eq, Clone, Ord, PartialOrd, Copy)]
enum Cards2 {
    A,
    K,
    Q,
    T,
    _9,
    _8,
    _7,
    _6,
    _5,
    _4,
    _3,
    _2,
    J,
}

impl Cards2 {
    pub fn iterator() -> Iter<'static, Cards2> {
        static DIRECTIONS: [Cards2; 13] = [Cards2::A, Cards2::K, Cards2::Q, Cards2::T, Cards2::_9, Cards2::_8, Cards2::_7, Cards2::_6, Cards2::_5, Cards2::_4, Cards2::_3, Cards2::_2, Cards2::J];
        DIRECTIONS.iter()
    }
}

impl From<char> for Cards2 {
    fn from(card: char) -> Self {
        match card {
            'A' => Cards2::A,
            'K' => Cards2::K,
            'Q' => Cards2::Q,
            'J' => Cards2::J,
            'T' => Cards2::T,
            '9' => Cards2::_9,
            '8' => Cards2::_8,
            '7' => Cards2::_7,
            '6' => Cards2::_6,
            '5' => Cards2::_5,
            '4' => Cards2::_4,
            '3' => Cards2::_3,
            '2' => Cards2::_2,
            _ => Cards2::_2,
        }
    }
}
#[derive(Debug, PartialEq, Eq, Clone, Ord, PartialOrd, Copy)]
enum Kind {
    Five,
    Four,
    FullHouse,
    Three,
    TwoPair,
    OnePair,
    High,
}

#[derive(Debug, Clone, Eq)]
struct Details {
    hand: Vec<Cards>,
    bet: i64,
    kind: Kind,
}

impl PartialEq<Self> for Details {
    fn eq(&self, other: &Self) -> bool {
        self.kind == other.kind && self.hand == other.hand
    }
}
impl Ord for Details {
    fn cmp(&self, other: &Self) -> Ordering {
        if self.kind == other.kind {
            for i in 0..5 {
                if self.hand[i] == other.hand[i] {
                    continue;
                }
                return self.hand[i].cmp(&other.hand[i]);
            }
            return Ordering::Equal;
        }
        self.kind.cmp(&other.kind)
    }
}

impl PartialOrd for Details {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

#[derive(Debug, Clone, Eq)]
struct Details2 {
    hand: Vec<Cards2>,
    bet: i64,
    kind: Kind,
}

impl PartialEq<Self> for Details2 {
    fn eq(&self, other: &Self) -> bool {
        self.kind == other.kind && self.hand == other.hand
    }
}

impl Ord for Details2 {
    fn cmp(&self, other: &Self) -> Ordering {
        if self.kind == other.kind {
            for i in 0..5 {
                if self.hand[i] == other.hand[i] {
                    continue;
                }
                return self.hand[i].cmp(&other.hand[i]);
            }
            return Ordering::Equal;
        }
        self.kind.cmp(&other.kind)
    }
}

impl PartialOrd for Details2 {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

#[derive(Debug)]
pub struct Processed {
    part1: Vec<Details>,
    part2: Vec<Details2>,
}

pub fn generator_1(input: &str) -> Processed {
    let mut processed = Processed { part1: vec![], part2: vec![] };

    for line in input.split('\n') {
        let mut details = Details { hand: vec![], bet: 0, kind: Kind::Five };
        let mut details2 = Details2 { hand: vec![], bet: details.bet, kind: Kind::Five };

        let mut split = line.split(' ');
        if let Some(hand) = split.next() {
            for card in hand.chars() {
                details.hand.push(Cards::from(card));
                details2.hand.push(Cards2::from(card));
            }
        }
        if let Some(bet) = split.next() {
            if let Ok(number) = bet.parse() {
                details.bet = number;
                details2.bet = number;
            }
        }

        details.kind = get_kind::<Cards>(&details.hand);
        details2.kind = get_kind_2(&details2.hand);

        processed.part1.push(details);
        processed.part2.push(details2);
    }

    processed
}

fn get_kind<T: Ord + Clone>(hand: &[T]) -> Kind {
    let mut tmp = hand.to_vec();
    tmp.sort();

    let mut kind_max = 0;
    let mut kind = 0;
    let mut last = &tmp[0];
    let mut pair = 0;

    for card in &tmp {
        if card == last {
            kind += 1;
        } else {
            if kind == 2 {
                pair += 1;
            }

            kind = 1;
            last = card;
        }
        if kind > kind_max {
            kind_max = kind;
        }
    }
    if kind == 2 {
        pair += 1;
    }

    //print!("{:?} {:?} {} {}", hand, tmp, kind, pair);

    if kind_max == 5 {
        return Kind::Five;
    }
    if kind_max == 4 {
        return Kind::Four;
    }
    if kind_max == 3 && pair == 1 {
        return Kind::FullHouse;
    }
    if kind_max == 3 {
        return Kind::Three;
    }
    if pair == 2 {
        return Kind::TwoPair;
    }
    if pair == 1 {
        return Kind::OnePair;
    }
    Kind::High
}

fn get_kind_2(hand: &Vec<Cards2>) -> Kind {
    let mut initial = get_kind::<Cards2>(hand);
    // println!("{:?}", hand);
    // println!("{:?}", initial);

    for tmp in Cards2::iterator() {
        let mut hand_test = vec![];
        for card in hand {
            if card == &Cards2::J {
                hand_test.push(*tmp);
            } else {
                hand_test.push(*card);
            }
        }

        let test_result = get_kind::<Cards2>(&hand_test);
        //println!("{:?} {:?}  {:?}", test_result , initial, initial.cmp(&test_result));

        if initial.cmp(&test_result).is_gt() {
            initial = test_result;
        }
    }

    initial
}

pub fn part1_1(input: &Processed) -> i64 {
    let mut local = input.part1.clone();
    local.sort();
    local.reverse();

    let mut counter = 0;
    for (i, hand) in local.iter().enumerate() {
        //println!("{:?}", hand);
        counter += (i + 1) as i64 * hand.bet;
    }
    counter
}

pub fn part2_1(input: &Processed) -> i64 {
    let mut local = input.part2.clone();
    local.sort();
    local.reverse();

    let mut counter = 0;
    for (i, hand) in local.iter().enumerate() {
        //println!("{:?}", hand);
        counter += (i + 1) as i64 * hand.bet;
    }
    counter
}

#[cfg(test)]
mod tests {
    use super::*;

    const EXAMPLE: &str = "32T3K 765
T55J5 684
KK677 28
KTJJT 220
QQQJA 483";

    #[test]
    fn part1_1_test() {
        assert_eq!(part1_1(&generator_1(EXAMPLE)), 6440);
    }

    #[test]
    fn part2_1_test() {
        assert_eq!(part2_1(&generator_1(EXAMPLE)), 5905);
    }
}
