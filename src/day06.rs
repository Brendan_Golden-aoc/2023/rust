#[derive(Debug)]
struct Details {
    time: Vec<i64>,
    distance: Vec<i64>,
}

#[derive(Debug)]
pub struct Processed {
    part1: Details,
    part2: Details,
}

pub fn generator_1(input: &str) -> Processed {
    let mut processed = Processed { part1: Details { time: vec![], distance: vec![] }, part2: Details { time: vec![], distance: vec![] } };

    let mut segments = input.split('\n');

    if let Some(line_time) = segments.next() {
        for part in line_time.split(' ') {
            if let Ok(number) = part.parse() {
                processed.part1.time.push(number);
            }
        }
        for part in line_time.replace(' ', "").split(':') {
            if let Ok(number) = part.parse() {
                processed.part2.time.push(number);
            }
        }
    }

    if let Some(line_distance) = segments.next() {
        for part in line_distance.split(' ') {
            if let Ok(number) = part.parse() {
                processed.part1.distance.push(number);
            }
        }
        for part in line_distance.replace(' ', "").split(':') {
            if let Ok(number) = part.parse() {
                processed.part2.distance.push(number);
            }
        }
    }

    processed
}

pub fn part1_1(input: &Processed) -> i64 {
    part1_sub(&input.part1)
}

fn part1_sub(details: &Details) -> i64 {
    let mut count = 1;

    for (i, time) in details.time.iter().enumerate() {
        let mut counter = 0;
        for speed in 1..*time {
            // old down for n seconds, thats yer new speed
            if (speed * (*time - speed)) > details.distance[i] {
                //println!("{}");

                counter += 1;
            }
        }
        count *= counter;
    }

    count
}

pub fn part2_1(input: &Processed) -> i64 {
    part1_sub(&input.part2)
}

pub fn part1_2(input: &Processed) -> i64 {
    sub_2(&input.part1)
}

pub fn part2_2(input: &Processed) -> i64 {
    sub_2(&input.part2)
}
fn sub_2(details: &Details) -> i64 {
    let mut count = 1;

    for (i, limit) in details.time.iter().enumerate() {
        let lim = *limit as f64;
        let y = details.distance[i] as f64;

        let sqrt = 0.5 * ((lim * lim) - (4.0 * y)).sqrt();
        let x_start = (0.5 * lim - sqrt).floor();
        let x_end = (0.5 * lim + sqrt).ceil();

        count *= (x_end - x_start - 1.0) as i64;
    }

    count
}

#[cfg(test)]
mod tests {
    use super::*;

    const EXAMPLE: &str = "Time:      7  15   30
Distance:  9  40  200";

    #[test]
    fn part1_test() {
        assert_eq!(part1_1(&generator_1(EXAMPLE)), 288);
    }

    #[test]
    fn part2_test() {
        assert_eq!(part2_1(&generator_1(EXAMPLE)), 71503);
    }

    #[test]
    fn part1_2_test() {
        assert_eq!(part1_2(&generator_1(EXAMPLE)), 288);
    }

    #[test]
    fn part2_2_test() {
        assert_eq!(part2_2(&generator_1(EXAMPLE)), 71503);
    }
}
