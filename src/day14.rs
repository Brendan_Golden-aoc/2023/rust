use std::collections::VecDeque;

#[derive(Debug)]
pub struct Processed {
    part1: Vec<Vec<char>>,
}

pub fn generator_1(input: &str) -> Processed {
    let mut processed = Processed { part1: vec![] };

    for line in input.split('\n') {
        processed.part1.push(line.chars().collect())
    }

    processed
}

pub fn part1_1(input: &Processed) -> usize {
    let rotated = left_rotate(&input.part1);
    let tilted = left_tilt(&rotated);

    left_load(&tilted)
}

pub fn part2_1(input: &Processed) -> usize {
    let mut board = left_rotate(&input.part1);

    let mut cycles = vec![];

    let cycles_test = 200;

    for _ in 0..cycles_test {
        cycles.push(left_load(&board));

        //println!("{} {}", i, left_load(&board));

        // north
        let north_tilt = left_tilt(&board);

        // west
        let west_rotate = right_rotate(&north_tilt);

        let west_tilt = left_tilt(&west_rotate);

        // south
        let south_rotate = right_rotate(&west_tilt);
        let south_tilt = left_tilt(&south_rotate);

        // east
        let east_rotate = right_rotate(&south_tilt);
        let east_tilt = left_tilt(&east_rotate);

        board = right_rotate(&east_tilt);
    }

    // going to start at teh back and work forwards
    cycles.reverse();

    let mut cycle_len = usize::MAX;

    'outer: for len in 3..cycles.len() {
        let mut windows = cycles.windows(len);
        let windows_first = windows.next().unwrap();

        'windows: for (index, window) in cycles.windows(len).enumerate() {
            if index == 0 {
                continue;
            }

            for i in 0..len {
                if window[i] != windows_first[i] {
                    continue 'windows;
                }
            }
            // found cyclke
            cycle_len = index;
            break 'outer;
        }
    }

    let mut cycle = cycles[0..cycle_len].to_owned();
    cycle.reverse();

    cycle[(1000000000 - cycles_test) % cycle_len]
}

fn left_tilt(board: &Vec<Vec<char>>) -> Vec<Vec<char>> {
    let mut result = vec![];

    for row in board {
        let mut row_new = vec![];
        let mut last_empty = VecDeque::new();

        for (position, character) in row.iter().enumerate() {
            row_new.push(*character);

            match character {
                '.' => last_empty.push_back(position),
                '#' => last_empty = VecDeque::new(),
                'O' => {
                    if let Some(empty) = last_empty.pop_front() {
                        row_new[empty] = 'O';
                        row_new[position] = '.';
                        last_empty.push_back(position);
                    }
                }
                _ => {}
            }
        }

        result.push(row_new);
    }
    result
}

fn left_rotate(board: &Vec<Vec<char>>) -> Vec<Vec<char>> {
    let mut result = vec![vec![' '; board.len()]; board[0].len()];

    for (i, line) in board.iter().enumerate() {
        for (j, character) in line.iter().enumerate() {
            result[board.len() - (1 + j)][i] = *character;
        }
    }

    result
}

fn right_rotate(board: &Vec<Vec<char>>) -> Vec<Vec<char>> {
    let mut result = vec![vec![' '; board.len()]; board[0].len()];
    for (i, line) in board.iter().enumerate() {
        for (j, character) in line.iter().enumerate() {
            result[j][board[0].len() - (1 + i)] = *character;
        }
    }
    result
}

fn left_load(board: &Vec<Vec<char>>) -> usize {
    let mut sum = 0;

    for row in board {
        for (position, character) in row.iter().enumerate() {
            if character == &'O' {
                sum += row.len() - position;
            }
        }
    }

    sum
}

#[cfg(test)]
mod tests {
    use super::*;

    const EXAMPLE: &str = "O....#....
O.OO#....#
.....##...
OO.#O....O
.O.....O#.
O.#..O.#.#
..O..#O..O
.......O..
#....###..
#OO..#....";

    #[test]
    fn part1_test() {
        assert_eq!(part1_1(&generator_1(EXAMPLE)), 136);
    }

    #[test]
    fn part2_test() {
        assert_eq!(part2_1(&generator_1(EXAMPLE)), 64);
    }
}
