use std::collections::HashSet;

#[derive(Debug)]
struct Details {
    springs: Vec<char>,
    records: Vec<usize>,
}

#[derive(Debug)]
pub struct Processed {
    part1: Vec<Details>,
}

pub fn generator_1(input: &str) -> Processed {
    let mut processed = Processed { part1: vec![] };

    for line in input.split('\n') {
        let mut details = Details { springs: vec![], records: vec![] };

        let mut line_details = line.split(' ');
        if let Some(springs) = line_details.next() {
            details.springs = springs.chars().collect();
        }

        if let Some(records) = line_details.next() {
            for record in records.split(',') {
                if let Ok(number) = record.parse() {
                    details.records.push(number);
                }
            }
        }

        processed.part1.push(details);
    }

    processed
}

pub fn part1_1(input: &Processed) -> usize {
    let mut counter = 0;

    for row in &input.part1 {
        counter += part1_sub(row);
    }

    counter
}

fn part1_sub(details: &Details) -> usize {
    let mut test = details.springs.clone();

    // 0, 1
    let mut positions = vec![];

    for (position, character) in test.iter().enumerate() {
        if character == &'?' {
            positions.push(position);
        }
    }

    let mut possible = HashSet::new();

    let test_len = test.len();
    for iteration in 0..2_usize.pow(test_len as u32) {
        let binary_tmp = format!("{:032b}", iteration);
        let binary = binary_tmp.chars().collect::<Vec<_>>();

        //println!("{:?}", binary);
        for (position, character) in test.iter_mut().enumerate() {
            if positions.contains(&position) {
                let position_back = test_len - position;
                //println!("position: {position} back: {position_back} ");

                let bin = binary[binary.len() - position_back];
                match bin {
                    '0' => {
                        *character = '.';
                    }
                    '1' => {
                        *character = '#';
                    }
                    _ => {}
                }
            }
        }
        possible.insert(test.clone().iter().collect::<String>());
    }

    let mut counter = 0;

    for testing in possible {
        if part1_test(&testing, &details.records) {
            counter += 1;
        }
    }

    counter
}

fn part1_test(testing: &str, criteria: &Vec<usize>) -> bool {
    let mut lists = vec![];
    let mut list = vec![];
    for character in testing.chars() {
        if character == '.' {
            if !list.is_empty() {
                lists.push(list);
                list = vec![];
            }
        } else {
            list.push(character);
        }
    }
    if !list.is_empty() {
        lists.push(list);
    }

    if lists.len() != criteria.len() {
        return false;
    }

    for (position, crietrion) in criteria.iter().enumerate() {
        if lists[position].len() != *crietrion {
            return false;
        }
    }

    true
}

pub fn part2_1(_input: &Processed) -> usize {
    0
}

#[cfg(test)]
mod tests {
    use super::*;

    const EXAMPLE: &str = "???.### 1,1,3
.??..??...?##. 1,1,3
?#?#?#?#?#?#?#? 1,3,1,6
????.#...#... 4,1,1
????.######..#####. 1,6,5
?###???????? 3,2,1";

    #[test]
    fn part1_test() {
        assert_eq!(part1_1(&generator_1(EXAMPLE)), 21);
    }

    #[test]
    fn part2_test1() {
        assert_eq!(part2_1(&generator_1(EXAMPLE)), 1);
    }
}
