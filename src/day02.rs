#[derive(Debug)]
struct Game {
    id: i64,
    red: i64,
    green: i64,
    blue: i64,
}

pub struct Processed {
    part1: Vec<Game>,
}

pub fn generator_1(input: &str) -> Processed {
    let mut processed = Processed { part1: vec![] };

    for line in input.split('\n') {
        let mut game = Game { id: 0, red: 0, green: 0, blue: 0 };

        let tmp = line.replace("Game ", "");
        let split = tmp.split(':').collect::<Vec<&str>>();
        game.id = split[0].parse::<i64>().unwrap();

        for draw in split[1].split(';').collect::<Vec<&str>>() {
            let mut cubes = (0, 0, 0);
            for cube in draw.split(',').collect::<Vec<&str>>() {
                let trimmed = cube.trim();
                let details = trimmed.split(' ').collect::<Vec<&str>>();
                let count = details[0].parse::<i64>().unwrap();
                match details[1] {
                    "red" => {
                        cubes.0 += count;
                    }
                    "green" => {
                        cubes.1 += count;
                    }
                    "blue" => {
                        cubes.2 += count;
                    }
                    &_ => {}
                }
            }
            if cubes.0 > game.red {
                game.red = cubes.0;
            }
            if cubes.1 > game.green {
                game.green = cubes.1;
            }
            if cubes.2 > game.blue {
                game.blue = cubes.2;
            }
        }

        processed.part1.push(game);
    }

    processed
}

pub fn part1_1(input: &Processed) -> i64 {
    let limits = (12, 13, 14);

    let mut count = 0;
    for game in &input.part1 {
        if game.red <= limits.0 && game.green <= limits.1 && game.blue <= limits.2 {
            count += game.id;
        }
    }

    count
}

pub fn part2_1(input: &Processed) -> i64 {
    let mut count = 0;
    for game in &input.part1 {
        count += game.blue * game.red * game.green;
    }

    count
}

#[cfg(test)]
mod tests {
    use super::*;

    const EXAMPLE: &str = "Game 1: 3 blue, 4 red; 1 red, 2 green, 6 blue; 2 green
Game 2: 1 blue, 2 green; 3 green, 4 blue, 1 red; 1 green, 1 blue
Game 3: 8 green, 6 blue, 20 red; 5 blue, 4 red, 13 green; 5 green, 1 red
Game 4: 1 green, 3 red, 6 blue; 3 green, 6 red; 3 green, 15 blue, 14 red
Game 5: 6 red, 1 blue, 3 green; 2 blue, 1 red, 2 green";

    #[test]
    fn part1_test() {
        assert_eq!(part1_1(&generator_1(EXAMPLE)), 8);
    }

    #[test]
    fn part2_test() {
        assert_eq!(part2_1(&generator_1(EXAMPLE)), 2286);
    }
}
