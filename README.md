![Stars Total](https://img.shields.io/endpoint?url=https://api.brendan.ie/aoc/get/1043391/all&style=) 
![Stars 2022](https://img.shields.io/endpoint?url=https://api.brendan.ie/aoc/get/1043391/2022) 
![Stars 2023](https://img.shields.io/endpoint?url=https://api.brendan.ie/aoc/get/1043391/2023) 

## Running It

I use https://github.com/remi-dupre/aoc to run my AoC solutions.  
The Windows config location: ``%appdata%/aoc/token.txt``

## Stars Badges

I created this tool/API https://gitlab.com/Brendan_Golden-aoc/aoc_badge_api   
Instructions in the readme.